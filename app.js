let kafka = require('kafka-node');
    
let Consumer = kafka.Consumer,
    consumer = new Consumer(
      client,
      [
        { topic: topicDemo, partition: 0 }
      ],
      {
        autoCommit: true
      }
    )

consumer.on('message', (message) => {
  console.log('Consumer got a message:')
  console.log(message)
})